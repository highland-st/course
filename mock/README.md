# mockjs

- 文档地址[http://mockjs.com/examples.html](http://mockjs.com/examples.html)

## 安装mockjs
```bash
npm i mockjs
```

## 示例
```js
// 字符
Mock.mock({
  "string|1-10": "★"
})
Mock.mock('@string(10)')

// 姓名
Mock.mock('@name')
Mock.mock('@cname')

// 数字
Mock.mock({
    'num|1-100': 100
})
Mock.mock('@integer(1,100)')

// 小数
Mock.mock({
    'num|1-100.3': 1
})
Mock.mock('@float(1,100,2,2)')

// boolean
Mock.mock('@boolean')
Mock.mock({
    'flag|1': [true, false]
})

// 时间
Mock.mock('@datetime')
Mock.Random.datetime('yyyy-MM-dd HH:mm:ss')

// 正则表达式
Mock.mock(/[a-z][A-Z][0-9]/)
// 身份证
Mock.mock(/^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[\dXx]$/)
// 手机号
Mock.mock(/^1[3456789]\d{9}/)

// 图片
Mock.Random.image('200x100')

// 数组
Mock.mock({
    'array|1-10': [
        {
            'id|+1': 1,
            name: '@cname',
            'role|1': ['管理员', '普通员工', '经理', 'HR'],
            age: '@integer(18, 35)',
            'weight|50-70.2': 50,
            job: /前端开发|技术经理|后端开发|产品/,
            height: /18|19|20|30|32/,
            idcard: /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[\dXx]$/,
            mobile: /1[3456789]\d{9}/,
            createTime: Mock.Random.datetime('yyyy-MM-dd HH:mm:ss'),
            sex: /男|女/,
            'sex2|1': ['男', '女']
        }
    ]
})

// 对象
const user = Mock.mock({
    'id|+1': 1,
    name: '@cname',
    'role|1': ['管理员', '普通员工', '经理', 'HR'],
    age: '@integer(18, 35)',
    'weight|50-70.2': 50,
    job: /前端开发|技术经理|后端开发|产品/,
    height: /18|19|20|30|32/,
    idcard: /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[\dXx]$/,
    mobile: /1[3456789]\d{9}/,
    createTime: Mock.Random.datetime('yyyy-MM-dd HH:mm:ss'),
    sex: /男|女/,
    'sex2|1': ['男', '女']
})

// 定义接口
Mock.mock('/api/user/info', 'get', () => {
    return Mock.mock({
        code: 200,
        success: true,
        message: '请求成功。',
        data: {
            'id|+1': 1,
            name: '@cname',
            'role|1': ['管理员', '普通员工', '经理', 'HR'],
            age: '@integer(18, 35)',
            'weight|50-70.2': 50,
            job: /前端开发|技术经理|后端开发|产品/,
            height: /18|19|20|30|32/,
            idcard: /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[\dXx]$/,
            mobile: /1[3456789]\d{9}/,
            createTime: Mock.Random.datetime('yyyy-MM-dd HH:mm:ss'),
            sex: /男|女/,
            'sex2|1': ['男', '女']
        }
    })
})
```