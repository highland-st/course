# 安装gitlab

## 更换yum镜像地址
```bash
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

## 更新缓存
yum makecache
```

## 安装基础依赖
```bash
yum install -y curl policycoreutils-python openssh-server perl postfix
# 启动邮件
systemctl enable postfix
systemctl start postfix
```

## 下载gitlab镜像
```bash
curl -O https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7/gitlab-ce-17.1.1-ce.0.el7.x86_64.rpm /usr/local
rpm -i gitlab-ce-17.1.1-ce.0.el7.x86_64.rpm
```

## gitlab配置
```bash
# /etc/gitlab/gitlab.rb
# 外部访问地址
external_url 'http://git.local.com:10000'
# nginx监听地址
nginx['listen_port'] = 10000
```

## gitlab命令
```bash
# 重载配置
gitlab-ctl reconfigure
gitlab-ctl restart
# 查看gitlab状态
gitlab-ctl status

# 查看初始密码
cat /etc/gitlab/initial_root_password
```
## 配置防火墙
```bash
# 开放端口
firewall-cmd --zone=public --add-port=10000/tcp --permanent 
 #关闭端口
firewall-cmd --zone=public --remove-port=10000/tcp --permanent 
# 配置立即生效
firewall-cmd --reload
 
# 查看防火墙所有开放的端口
firewall-cmd --zone=public --list-ports

# 关闭防火墙
systemctl stop firewalld.service

# 查看防火墙状态
firewall-cmd --state
```