# unocss
### 安装
```bash
npm install unocss@0.58.9 -D
```
### 文档
[https://unocss.nodejs.cn](https://unocss.nodejs.cn/)<br/>
[https://tailwind.nodejs.cn/docs](https://tailwind.nodejs.cn/docs)<br/>
### vite集成unocss
```js
// vite.config.js
import UnoCSS from 'unocss/vite'
import { defineConfig } from 'vite'

export default defineConfig(({ mode, command }) => {
	return {
	  plugins: [
	    UnoCSS(),
	  ],
	}
})

// main.js
import 'virtual:uno.css'
```
### 使用默认预设
```js
// ### uno.config.js
import { defineConfig, presetUno } from 'unocss'

export default defineConfig({
  presets: [
    presetUno(),
  ],
})
```
