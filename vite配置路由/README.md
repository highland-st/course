# vue-router
- 官方文档[https://router.vuejs.org/zh/](https://router.vuejs.org/zh/)

# 安装
```bash
npm install vue-router
```

```js
// src/router/index.js
import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    children: [
	  {
	    path: 'home/index',
	    component: () => import('../views/home/index.vue'),
	    meta: { title: '首页' }
	  },
    ]
  },
  {
    path: '/login',
    component: () => import('../views/login/index.vue'),
    meta: { title: '登录' }
  },
  {
    path: '/:pathMatch(.*)*',
    component: () => import('../views/error/404/index.vue'),
    meta: { title: '404' }
  }
]
const router = createRouter({
  history: createWebHashHistory(),
  routes
})

// 路由导航守卫
router.beforeEach((to, from) => {
	return true
})

export function setup(app) {
  app.use(router)
}
```

```js
// main.js
import { createApp } from 'vue'
import App from './App.vue'
imoprt { setup as setupRouter } from '@/router/index.js'

const app = createApp(App)
setupRouter(app)
app.mount('#app')

```