# chrome安装Markdown Viewer插件
[插件下载地址 https://www.kuozhanmi.com/sites/7527.html](https://www.kuozhanmi.com/sites/7527.html)
## chrome安装插件的教程
[教程地址 https://www.kuozhanmi.com/sites/189.html](https://www.kuozhanmi.com/sites/189.html)

## 插件安装好后，进入插件详情，打开图中的开关
![img.png](img.png)

## 将md文档拖拽到chrome浏览器即可预览，或者设置md文档打开方式为chrome浏览器