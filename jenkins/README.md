# jenkins持续集成，实现自动化部署

## 文档
[https://www.jenkins.io/](https://www.jenkins.io/) <br/>
下载地址<br/>
[https://get.jenkins.io/war/](https://get.jenkins.io/war/)

## 启动jenkins
```bash
nohup /usr/local/java17/bin/java -jar /usr/local/jenkins-2.462.1.war --httpPort=9002 &
```

## 修改插件镜像地址
```bash
sed -i 's/https:\/\/updates.jenkins.io\/download/https:\/\/mirrors.tuna.tsinghua.edu.cn\/jenkins/g' /root/.jenkins/updates/default.json && sed -i 's/http:\/\/www.google.com/https:\/\/www.baidu.com/g' /root/.jenkins/updates/default.json
```

## 重启jenkins
```bash
# 浏览器地址栏访问
http://192.168.1.36:9002/restart
```

## jenkins脚本
```bash
import java.text.SimpleDateFormat
pipeline {
    agent any

    options {
        timestamps()
    }
    parameters {
        choice(name: 'Run',choices: ['build', 'copy', ],description: '部署类型 (copy:直接复制上次部署的文件, build:重新构建)')
        booleanParam(name: 'Base', defaultValue: true, description: '部署基础模块')
        booleanParam(name: 'Install', defaultValue: false, description: '下载新的依赖')
        choice(name: 'Env',choices: ['dev', 'test', ],description: '部署环境')
    }
    stages {
        stage('构建基础模块代码') {
            when { expression { return params.Base } }
            steps {
                echo "=========================构建基础模块代码========================="
                script{
                    if ("${params.Run}" == "copy") {
                        echo "copy"
                    }else{
                        echo "Build"
                        if("${params.Install}" == "true") {
                        	sh "rm -rf node_modules"
                        	sh "npm install --registry=https://registry.npmmirror.com"
                        }
                        sh "rm -rf dist"
                        sh "npm run build:${params.Env}"
                        sh "cp -r ./dist/* /usr/share/nginx/html"
                    }
                }
            }
        }
        stage('构建镜像') {
            steps {
                echo "=========================构建镜像========================="
                //script{
                    //sh "docker build -t 127.0.0.1:8082/base-system/nginx:dev -f ./Dockerfile ."
                //}
            }
        }
        stage('推送镜像') {
            steps {
                echo "=========================推送镜像========================="
                //script{
                    //sh "docker login https://127.0.0.1:8082 -u admin -p QWEasd123"
                    //sh "docker push 127.0.0.1:8082/base-system/nginx:${params.Env}"
                //}
            }
        }

        stage(" dev环境部署"){
            when { expression { return params.Env == "dev" } }
            steps {
                echo "部署dev镜像"
           }
        }

        stage(" test环境部署"){
            when { expression { return params.Base == "test" } }
            steps {
                echo "部署test镜像"
            }
        }
    }
}
```