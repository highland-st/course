# Vite集成Svg图标

## 安装
```bash
npm i vite-plugin-svg-icons -D
```

## Vite配置
```js
// vite配置
import { createSvgIconsPlugin } from "vite-plugin-svg-icons"
import path from "path"
import { defineConfig } from "vite"

export default defineConfig(({ mode, command }) => {
	return {
    plugins: [
    	createSvgIconsPlugin({
          iconDirs: [path.resolve(__dirname, "src/assets/icons")],
          symbolId: "icon-[name]",
    	}),
    ]
	}
})
```

## Svg图标组件
```js
<template>
  <svg aria-hidden="true" class="svg-icon">
    <use :xlink:href="symbolId" :fill="color" />
  </svg>
</template>

<script setup>
import { computed } from "vue"

const props = defineProps({
  prefix: {
    type: String,
    default: "icon",
  },
  name: {
    type: String,
    default: "",
  },
  color: {
    type: String,
    default: "",
  },
})

const symbolId = computed(() => {
  return `#${props.prefix}-${props.name}`
})
</script>

<style scoped>
.svg-icon {
  width: 1em;
  height: 1em;
  vertical-align: -0.15em;
  fill: currentColor;
  overflow: hidden;
}
</style>
```

```js
// 注册全局组件
import SvgIcon from "@/components/svg-icon/index.vue"
import "virtual:svg-icons-register"

export function setup(app) {
  app.component("SvgIcon", SvgIcon)
}
```

## 常用图标库
- [谷歌图标库](https://fonts.google.com/icons)
- [xicons](https://www.xicons.org/#/)
- [字节跳动图标库](https://iconpark.oceanengine.com/official)
- [icones](https://icones.js.org/)
- [阿里图标库](https://www.iconfont.cn/)
- [fontawesome](https://fontawesome.com/)
- [figma图标库-1](https://www.figma.com/design/2xf9Xn5UXxyk849RWhX84O/Free-Icon-Pack-1800%2B-icons-(Community)?node-id=2-27&t=fonaNyNUyPzwAb3I-1)
- [figma图标库-2](https://www.figma.com/design/TnJmhXOegRvfyUsnOScg5M/MynaUI-Icons---Free-Open-Source-Icons-(Community)?node-id=0-1&t=z90cVVzD49C7X84f-1)
- [figma图标库-3](https://www.figma.com/design/MsN9yZEnJwaeKCpN6hMFb0/4%2C000-Free-Icons---Open-Source-Icon-set-(Community)?node-id=1-3&t=NHGdxGHLhenwdK2y-1)