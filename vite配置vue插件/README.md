# Vue
- 官方文档[https://cn.vuejs.org/](https://cn.vuejs.org/)

# 安装
```bash
npm install vue
```
```js
// main.js
import { createApp } from 'vue'
import App from './App.vue'

const app = createApp(App)
app.mount('#app')
```

```js
// 设置全局组件
app.component('ComponentName', Component)
// 安装一个插件
app.use(plugin)
```

# 在vite中安装vue插件
```bash
npm install @vitejs/plugin-vue -D
```
```js
// vite.config.js
import vue from '@vitejs/plugin-vue'
defineConfig(() => {
	return {
		plugins: [
			vue()
	    ]
	}
})
```
