# 集成Element-plus

## 文档
[https://element-plus.org/zh-CN/guide/design.html](https://element-plus.org/zh-CN/guide/design.html)
## 安装
```bash
npm install element-plus
```
## 安装svg图标
```bash
npm install @element-plus/icons-vue
```
## 引用
```js
import {
  ElAlert,
  ElAside,
  ElAutocomplete,
  ElAvatar,
  ElBacktop,
  ElBadge,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElButton,
  ElButtonGroup,
  ElCalendar,
  ElCard,
  ElCarousel,
  ElCarouselItem,
  ElCascader,
  ElCascaderPanel,
  ElCheckbox,
  ElCheckboxButton,
  ElCheckboxGroup,
  ElCol,
  ElCollapse,
  ElCollapseItem,
  ElCollapseTransition,
  ElColorPicker,
  ElContainer,
  ElDatePicker,
  ElDialog,
  ElDivider,
  ElDrawer,
  ElDropdown,
  ElDropdownItem,
  ElDropdownMenu,
  ElEmpty,
  ElFooter,
  ElForm,
  ElFormItem,
  ElHeader,
  ElIcon,
  ElImage,
  ElInput,
  ElInputNumber,
  ElLink,
  ElMain,
  ElMenu,
  ElMenuItem,
  ElMenuItemGroup,
  ElOption,
  ElOptionGroup,
  ElPageHeader,
  ElPopconfirm,
  ElPopover,
  ElPopper,
  ElProgress,
  ElRadio,
  ElRadioButton,
  ElRadioGroup,
  ElRate,
  ElRow,
  ElScrollbar,
  ElSelect,
  ElSlider,
  ElStep,
  ElSteps,
  ElSwitch,
  ElSpace,
  ElTabPane,
  ElTable,
  ElTableColumn,
  ElTabs,
  ElTag,
  ElTimePicker,
  ElTimeSelect,
  ElTimeline,
  ElTimelineItem,
  ElTooltip,
  ElTransfer,
  ElTree,
  ElTreeSelect,
  ElUpload,
  ElInfiniteScroll,
  ElLoading,
  ElMessage,
  ElPagination,
  ElMessageBox,
  ElNotification
} from 'element-plus'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const components = [
  ElAlert,
  ElAside,
  ElAutocomplete,
  ElAvatar,
  ElBacktop,
  ElBadge,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElButton,
  ElButtonGroup,
  ElCalendar,
  ElCard,
  ElCarousel,
  ElCarouselItem,
  ElCascader,
  ElCascaderPanel,
  ElCheckbox,
  ElCheckboxButton,
  ElCheckboxGroup,
  ElCol,
  ElCollapse,
  ElCollapseItem,
  ElCollapseTransition,
  ElColorPicker,
  ElContainer,
  ElDatePicker,
  ElDialog,
  ElDivider,
  ElDrawer,
  ElDropdown,
  ElDropdownItem,
  ElDropdownMenu,
  ElEmpty,
  ElFooter,
  ElForm,
  ElFormItem,
  ElHeader,
  ElIcon,
  ElImage,
  ElInput,
  ElInputNumber,
  ElLink,
  ElMain,
  ElMenu,
  ElMenuItem,
  ElMenuItemGroup,
  ElOption,
  ElOptionGroup,
  ElPageHeader,
  ElPagination,
  ElPopconfirm,
  ElPopover,
  ElPopper,
  ElProgress,
  ElRadio,
  ElRadioButton,
  ElRadioGroup,
  ElRate,
  ElRow,
  ElScrollbar,
  ElSelect,
  ElSlider,
  ElStep,
  ElSteps,
  ElSwitch,
  ElSpace,
  ElTabPane,
  ElTable,
  ElTableColumn,
  ElTabs,
  ElTag,
  ElTimePicker,
  ElTimeSelect,
  ElTimeline,
  ElTimelineItem,
  ElTooltip,
  ElTransfer,
  ElTree,
  ElTreeSelect,
  ElUpload,
  ElInfiniteScroll,
  ElLoading,
  ElMessage,
  ElMessageBox,
  ElNotification
]

// ElTable
const TableProps = ElTable.props
const TableColumnProps = ElTableColumn.props
TableProps.border = { type: Boolean, default: false } // 边框线
// TableProps.stripe = { type: Boolean, default: true } // 斑马纹
TableProps.size = { type: String, default: 'small' } // 表格内容大小
TableProps.headerRowClassName = { type: String, default: 'table-header-row' } // 设置row的classname
TableProps.headerCellClassName = { type: String, default: 'table-header-cell' } // 设置cell的classname
TableColumnProps.align = { type: String, default: 'center' } // 居中
TableColumnProps.showOverflowTooltip = { type: Boolean, default: true } // 文本溢出

// ElInput
const ElInputProps = ElInput.props
ElInputProps.clearable = { type: Boolean, default: true }
// ElInputProps.placeholder = { type: String, default: '请输入'}
ElInputProps.maxlength = { type: Number, default: 50 }

// ElSelect
const ElSelectProps = ElSelect.props
ElSelectProps.clearable = { type: Boolean, default: true }
ElSelectProps.filterable = { type: Boolean, default: true }
ElSelectProps.placeholder = { type: String, default: '请选择'}

// ElDialog
const ElDialogProps = ElDialog.props
ElDialogProps.closeOnClickModal = { type: Boolean, default: false }
ElDialogProps.width = { type: String, default: '850px' }

// ElDrawer
const ElDrawerProps = ElDrawer.props
ElDrawerProps.closeOnClickModal = { type: Boolean, default: false }
ElDrawerProps.size = { type: String, default: '400px' }

// ElPagination
const ElPaginationProps = ElPagination.props
ElPaginationProps.pageSizes = { type: Array, default: () => [10, 20, 30, 50] }
ElPaginationProps.size = { type: Number, default: 10 }
ElPaginationProps.background = { type: Boolean, default: true }
ElPaginationProps.layout = { type: String, default: 'total, sizes, prev, pager, next' }
ElPaginationProps.size = { type: String, default: 'small' }

// ElDate
const ElTimePickerProps = ElTimePicker.props
const ElDatePickerProps = ElDatePicker.props
ElTimePickerProps.clearable = { type: Boolean, default: false }
ElDatePickerProps.clearable = { type: Boolean, default: false }

export function setup(app) {
  // 全局注册组件
  components.forEach(component => {
    app.use(component)
  })
  // 全局注册Icon
  for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
  }
  // 全局挂载
  if (window) {
    window.$notify = ElNotification
    window.$message = ElMessage
    window.$confirm = (message, title, options) => {
      if (!options.closeOnClickModal) {
        options.closeOnClickModal = false
      }
      return ElMessageBox.confirm(message, title, options)
    }
    window.$alert = ElMessageBox.alert
  }
}
```

```html
<!-- 配置语言 -->
<template>
  <el-config-provider :locale="zhCn" size="default" :z-index="200">
    <router-view />
  </el-config-provider>
</template>
<script setup>
import { ElConfigProvider } from "element-plus"
import zhCn from "element-plus/dist/locale/zh-cn.mjs"
</script>
```

## 引用element-plus样式
```scss
@forward 'element-plus/theme-chalk/src/common/var.scss' with (
  $colors: (
    'primary': (
      'base': green,
    ),
  ),
);
@import 'element-plus/theme-chalk/src/index.scss';
```