# 配置scss全局变量
## 安装
```bash
npm install sass -D
```
## vite配置scss全局变量
```js
// vite.config.js
import {defineConfig} from 'vite'
export default defineConfig(({command, mode, isSsrBuild, isPreview}) => {
    if (command === 'serve') {
        console.log('is serve')
    } else {
        console.log('is build')
    }
    return {
        // 全局Scss变量
        css: {
            preprocessorOptions: {
                scss: {
                    additionalData: `@import 'src/styles/define.scss';`
                }
            }
        }
    }
})
```
## scss全局变量
```scss
/* 行为相关颜色 */
$--primary-color: #3298F8;
$--success-color: #4cd964;
$--warning-color: #f0ad4e;
$--error-color: #f56c6c;
$--danger-color: #f56c6c;
$--info-color: #909399;
$--secondary-color: #fb8748;

@mixin ellipsis() {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

@mixin line-clamp-2() {
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
  overflow: hidden;
}

@mixin line-clamp-3() {
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 3;
  overflow: hidden;
}

@mixin line-clamp($num) {
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: $num;
  overflow: hidden;
}

@mixin scrollbar($height: 0, $width: 8px) {
  @if $height != 0 {
    height: $height;
  }
  overflow-x: hidden;
  overflow-y: auto;
  &::-webkit-scrollbar {
    width: $width;
    position: absolute;
  }
  &::-webkit-scrollbar-thumb {
    border-radius: 10px;
    transition: all 0.5s;
  }
  &:hover {
    &::-webkit-scrollbar-thumb {
      background-color: rgba(114, 114, 114, 0.5);
    }
  }
}
```