# js调用摄像头实现拍照

## 一寸照片 25mm×35mm

## 调用摄像头
```js
const stream = await navigator.mediaDevices.getUserMedia({ video: true });
// 处理异常 NotAllowedError | PermissionDeniedError | NotFoundError | DevicesNotFoundError | Device in use
```

## base64转file
```js
function base64ToFile(base64) {
  let arr = base64.split(",");
  let contentType = arr[0].match(/:(.*?);/)[1];
  let bstr = atob(arr[1]);
  let n = bstr.length;
  let u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  const blob = new Blob([u8arr], { type: contentType });
  return new File([blob], name);
}
```
## canvas截图
```js
function cutImg() {
  const canvas = document.createElement("canvas");
  canvas.width = 275;
  canvas.height = 360;
  canvas.getContext("2d").drawImage(videoRef.value, 182, 0, 275, 360, 0, 0, 275, 360);
  const base64 = canvas.toDataURL("image/jpeg", 1);
}
```