# Docker容器技术

## 修改docker镜像地址
文件位置/etc/docker/daemon.json，没有的话需要新建一个
```json
{
  "registry-mirrors": [
    "https://dockerhub.icu",
    "https://docker.mirrors.ustc.edu.cn",
    "https://docker.nju.edu.cn",
    "https://docker.1panel.dev"
  ]
}
```
```bash
# 重载配置文件
systemctl daemon-reload
# 重启 Docker 服务
systemctl restart docker
```

## 下载nginx镜像
```bash
# 拉取nginx镜像
docker pull nginx
# 查看镜像
docker images
# 删除镜像
docker rmi nginx
```

## 使用基础镜像创建项目镜像
```bash
#Dockerfile
FROM nginx:latest
COPY demo.nginx.conf  /etc/nginx/conf.d/
COPY dist  /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
```

## 构建镜像
```bash
docker build -t demo:dev -f ./Dockerfile .
```

## 操作容器
```bash
# 使用制作的项目镜像启动容器
docker run -d -p 9003:80 --name demo-dev nginx:latest #换成自己项目的镜像名称
# 查看运行的容器
docker ps
docker ps -a
# 停止容器
docker stop demo-dev
# 启动停止的容器
docker start demo-dev
# 删除容器
docker rm demo-dev
# 进入容器内部
docker exec -it demo-dev /bin/bash
```

```bash
# jenkinsfile
import java.text.SimpleDateFormat
pipeline {
    agent any

    options {
        timestamps()
    }
    parameters {
        choice(name: 'Run',choices: ['build', 'copy', ],description: '部署类型 (copy:直接复制上次部署的文件, build:重新构建)')
        booleanParam(name: 'Base', defaultValue: true, description: '部署基础模块')
        booleanParam(name: 'Install', defaultValue: false, description: '下载新的依赖')
        choice(name: 'Env',choices: ['dev', 'test', ],description: '部署环境')
    }
    stages {
        stage('构建基础模块代码') {
            when { expression { return params.Base } }
            steps {
                echo "=========================构建基础模块代码========================="
                script{
                    if ("${params.Run}" == "copy") {
                        echo "copy"
                    }else{
                        echo "Build"
                        if("${params.Install}" == "true") {
                        	sh "rm -rf node_modules"
                        	sh "npm install --registry=https://registry.npmmirror.com"
                        }
                        sh "rm -rf dist"
                        sh "npm run build:${params.Env}"
                        // sh "cp -r ./dist/* /usr/share/nginx/html"
                    }
                }
            }
        }
        stage('构建镜像') {
            steps {
                echo "=========================构建镜像========================="
                script{
                    sh "docker build -t demo:${params.Env} -f ./Dockerfile ."
                    echo "=========================停止容器========================="
                    try {
                        sh "docker stop demo-${params.Env}"
                        sh "docker rm demo-${params.Env}"
                    }catch(e){

                    }
                    echo "=========================启动镜像========================="
                    sh "docker run -d -p 9003:80 --name demo-${params.Env} demo:${params.Env}"
                }
            }
        }
        stage('推送镜像') {
            steps {
                echo "=========================推送镜像========================="
                //script{
                    //sh "docker login https://127.0.0.1:8082 -u admin -p QWEasd123"
                    //sh "docker push 127.0.0.1:8082/base-system/nginx:${params.Env}"
                //}
            }
        }

        stage(" dev环境部署"){
            when { expression { return params.Env == "dev" } }
            steps {
                echo "部署dev镜像"
           }
        }

        stage(" test环境部署"){
            when { expression { return params.Base == "test" } }
            steps {
                echo "部署test镜像"
            }
        }
    }
}
```

```bash
# demo.nginx.conf
server {
    listen 80 default_server;
    client_max_body_size 201m;
    #access_log /var/log/host.access.log main;
    gzip on;
    gzip_min_length 100;
    gzip_buffers 32 4K;
    gzip_comp_level 5;
    gzip_types text/plain application/javascript application/x-javascript text/css application/xml text/javascript application/x-httpd-php image/jpeg image/gif image/png;
    #配置禁用gzip条件，支持正则。此处表示ie6及以下不启用gzip（因为ie低版本不支持）
    gzip_disable "MSIE [1-6]\.";
    gzip_vary on;

    location / {
        root /usr/share/nginx/html;
		index index.html index.htm;
    }

    #location /api {
    #   proxy_pass
    #}
}
```