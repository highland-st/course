# Vite环境变量

```bash
# .env
VITE_TITLE = 'Vite demo'

VITE_BASE_API = '/api'

VITE_APP_VERSIOIN = '1.0.0'

VITE_LOGO = ''

```

```bash
# .env.development
VITE_ENV = 'development'

VITE_BASE_URL = 'http://127.0.0.1:8080'

```

```bash
# .env.test
VITE_ENV = 'test'

VITE_BASE_URL = 'http://127.0.0.1:8081'

```

```bash
# .env.production
VITE_ENV = 'production'

VITE_BASE_URL = 'https://example.com'

```


```js
// vite.config.js使用环境变量
import { defineConfig, loadEnv } from 'vue'

defineConfig(({mode}) => {
	const env = loadEnv(mode, process.cwd())
})

```

```js
// 内置默认环境变量
import.meta.env.MODE
import.meta.env.PROD // 是否是build打包
import.meta.env.DEV // 是否是server本地启动

// 判断开发环境
const isDev = import.meta.env.MODE === 'development'
if(isDEV) {
	console.log('dev执行的代码')
}else {
	console.log('==')
}

// 使用自定义环境变量
import.meta.env.VITE_ENV
import.meta.env.VITE_TITLE
```

```html
<!-- html中使用变量 -->
<html>
	<head>
		<title>%VITE_TITLE%</title>
	</head>
	<body></body>
</html>
```