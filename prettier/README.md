# prettier代码美化工具

```bash
npm install prettier -D
```
## 官方文档
[https://prettier.io/](https://prettier.io/)

## 配置文件
```js
// .prettierrc
{
	// 每行代码数量
  "printWidth": 120,
  // 缩进的空格个数
  "tabWidth": 2,
  // 制表符使用空格
  "useTabs": false,
  // 末尾分号
  "semi": false,
  // 换行符
  "endOfLine": "auto",
  // 单引号
  "singleQuote": true,
  // 对象末尾始终携带逗号
  "trailingComma": "all",
  // 对象大括号带空格
  "bracketSpacing": true,
  // 箭头符号参数始终带括号
  "arrowParens": "always",
}

// .prettierignore
dist
node_modules
.eslintignore
.prettierignore
```

## 格式化命令
```bash
prettier --write src/
```