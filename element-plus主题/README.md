## 前端主题切换

### 深色主题
```scss
// styles/dark.scss
@forward 'element-plus/theme-chalk/src/dark/var.scss' with (
  $bg-color: (
    //'page': #181818,
    '': #202020,
    'overlay': #1d1e1f,
  ),
  $colors: (
    'primary': (
      'base': $--primary-color,
    ),
  )
);
@import 'element-plus/theme-chalk/src/dark/css-vars.scss';

html.dark {
  --card-bg: #2a2a2a;
}
```

### 浅色主题
```scss
// styles/light.scss
@use 'sass:map';
@use 'sass:math';

html.light {
  --card-bg: white;
  $el-bg: white;

  $colors: (
          'white': #ffffff,
          'black': #000000,
          'primary': #094fd2,
          'success': #67c23a,
          'warning': #e6a23c,
          'danger': #f56c6c,
          'error': #f56c6c,
          'info': #909399,
  );
  $color-black: map.get($colors, 'black');
  --el-color-black: #{$color-black};
  @each $type in (primary, success, warning, danger, error, info) {
    $color: map.get($colors, $type);
    --el-color-#{$type}: #{$color};
    @each $i in (3, 5, 7, 8, 9) {
      $weight: math.percentage(math.div($i, 10));
      $value: mix($el-bg, $color, $weight);
      --el-color-#{$type}-light-#{$i}: #{$value};
    }
    --el-color-#{$type}-dark-2: #{mix(#ffffff, $color, math.percentage(math.div(2, 10)))};
  }
}
```