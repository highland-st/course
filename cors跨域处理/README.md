# 创建http服务
```js
// node http服务
// https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js
const http = require('node:http')

const server = http.createServer()
server.on('request', (request, response) => {
  const origin = request.headers.origin
  const originList = [
    'http://localhost:9999',
    'http://localhost:80',
    'http://localhost:9527'
  ]
  if(origin) {
    const has = originList.includes(origin)
    if(has) {
      response.setHeader('Access-Control-Allow-Origin', origin)
      response.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS')
      response.setHeader('Access-Control-Allow-Headers', 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization,Token,AppId')
      if(request.method === 'OPTIONS') {
        response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end('200')
        return
      }
    }
  }
  response.writeHead(200, { 'Content-Type': 'application/json;charset=UTF-8' })
  response.end(JSON.stringify({
    code: 'success',
    data: 'Hello World!',
  }))
})
server.listen(8080, () => {
  console.log('server started')
})
```

# nginx代理配置
```bash
server {
  listen 80;
  server_name  dev.local.com;

  location /test {
    #add_header Access-Control-Allow-Credentials true;
    #add_header Access-Control-Allow-Origin 'http://localhost';
    #add_header Access-Control-Allow-Methods 'GET, POST, PUT, DELETE, OPTIONS';
    #add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';
    #if ($request_method = 'OPTIONS') {
    #    return 200;
    #}
    add_header Content-Type 'application/json;charset=UTF-8';
    return 200 "{\"code\": \"success\", \"data\": \"你好！\"}";
  }
  
  location /api {
    proxy_pass http://localhost:8080;
  }
}
```

# vite代理配置
```js
import {defineConfig, loadEnv} from 'vite';
import path from 'node:path'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import {createSvgIconsPlugin} from "vite-plugin-svg-icons"

export default defineConfig(({mode, command}) => {
  return {
    server: {
      port: 8000,
      host: '0.0.0.0',
      open: true,
      proxy: {
        '/api': {
          target: 'http://localhost:8080',
          changeOrigin: true,
          ws: true
          // rewrite: path => path.replace(new RegExp(`^/api`), '')
        }
      }
    },
  }
})
```